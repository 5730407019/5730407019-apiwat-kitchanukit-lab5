package wow1;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class DOMTraversal {

	public static void main(String[] args) {
		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder parser = factory.newDocumentBuilder();

			Document xmlF = parser.parse(new File(
					"C:\\Users\\pc1\\Downloads\\nation.xml"));
			Node node = xmlF.getFirstChild();
			followNode(node);

		} catch (Exception e) {
			System.out.println("Error");
		}

	}

	public static void followNode(Node node) throws IOException {
		writeNode(node);
		if (node.hasChildNodes()) {
			String name = node.getNodeName();
			int numChildren = node.getChildNodes().getLength();
			System.out.println("node " + name + " has " + numChildren
					+ " Children");
			Node firstChild = node.getFirstChild();
			followNode(firstChild);

		}
		Node nextNode = node.getNextSibling();
		if (nextNode != null)
			followNode(nextNode);
	}

	private static void writeNode(Node node) {

		System.out.print("Node:type = " + node.getNodeType() + " name = "
				+ node.getNodeName() + " value = " + node.getNodeValue());
	}

}
